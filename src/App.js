import "./App.css";
import Banner from "./Pages/Banner";
import { Parallax, ParallaxLayer } from "@react-spring/parallax";
import Content from "./Pages/Content";
import { useRef } from "react";
import ReactAudioPlayer from "react-audio-player";

function App() {
  return (
    <div className="App">
      <Parallax pages={2}>
        <ParallaxLayer offset={0}>
          <Banner></Banner>
        </ParallaxLayer>
        <ParallaxLayer offset={1}>
          <Content></Content>
        </ParallaxLayer>
        <ReactAudioPlayer
          src="Let It Snow - Frank Sinatra.mp3"
          autoPlay={true}
          // controls
        />
      </Parallax>
    </div>
  );
}

export default App;
